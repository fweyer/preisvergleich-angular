import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KontaktanfrageseiteComponent } from 'src/app/menuComponents/kontaktanfrageseite/kontaktanfrageseite.component'
import { UnternehmenseiteComponent } from 'src/app/menuComponents/unternehmenseite/unternehmenseite.component'
import { PreisseiteComponent } from './menuComponents/preisseite/preisseite.component';

const routes: Routes = [
  { path: '', redirectTo: 'preise', pathMatch: 'full' },
  { path: 'kontaktseite', component: KontaktanfrageseiteComponent },
  { path: 'unternehmenseite', component: UnternehmenseiteComponent },
  { path: 'preise', component: PreisseiteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
