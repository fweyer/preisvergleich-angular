import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { KontaktanfragenDTO } from '../models/KontaktanfragenDTO'
import PreisvergleichService from '../services/preisvergleichservice.interface'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class KontaktanfrageService implements PreisvergleichService<KontaktanfragenDTO> {

  private url = 'http://localhost:8080/admin'
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };


  constructor(private http: HttpClient) { }
  getData(): Observable<KontaktanfragenDTO[]> {
    return this.http.get<KontaktanfragenDTO[]>(this.url + "/ladeKontakte")
  }

  putData(t: KontaktanfragenDTO) {
    return this.http.post<KontaktanfragenDTO>(this.url + "/fuegeKontakteHinzu", t, this.httpOptions)

  }

  changeData(t: KontaktanfragenDTO) {
    return this.http.post<KontaktanfragenDTO>(this.url + "/aendereKontakte", t, this.httpOptions)

  }

  deleteData(t: KontaktanfragenDTO) {
    return this.http.post<KontaktanfragenDTO>(this.url + "/loescheKontakte", t, this.httpOptions)
  }

}
