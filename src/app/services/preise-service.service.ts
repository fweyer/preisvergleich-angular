import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PreiseDTO } from '../models/PreiseDTO'
import PreisvergleichService from '../services/preisvergleichservice.interface'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PreiseService implements PreisvergleichService<PreiseDTO> {


  private url = 'http://localhost:8080/admin'
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };


  constructor(private http: HttpClient) { }
  getData(): Observable<PreiseDTO[]> {
    return this.http.get<PreiseDTO[]>(this.url + "/ladePreise")
  }

  putData(t: PreiseDTO) {
    return this.http.post<PreiseDTO>(this.url + "/fuegePreiseHinzu", t, this.httpOptions)

  }

  changeData(t: PreiseDTO) {
    return this.http.post<PreiseDTO>(this.url + "/aenderePreise", t, this.httpOptions)

  }

  deleteData(t: PreiseDTO) {
    return this.http.post<PreiseDTO>(this.url + "/loeschePreise", t, this.httpOptions)
  }

}
