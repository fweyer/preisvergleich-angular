
export default interface PreisvergleichService<T> {
    getData()

    putData(t: T)

    changeData(t: T)

    deleteData(t: T)

}