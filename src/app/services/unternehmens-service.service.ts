import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UnternehmenDTO } from '../models/UnternehmenDTO'
import PreisvergleichService from '../services/preisvergleichservice.interface'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UnternehmensService implements PreisvergleichService<UnternehmenDTO> {

  private url = 'http://localhost:8080/admin'
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };


  constructor(private http: HttpClient) { }
  getData(): Observable<UnternehmenDTO[]> {
    return this.http.get<UnternehmenDTO[]>(this.url + "/ladeUnternehmen")
  }

  putData(t: UnternehmenDTO) {
    return this.http.post<UnternehmenDTO>(this.url + "/fuegeUnternehmenHinzu", t, this.httpOptions)

  }

  changeData(t: UnternehmenDTO) {
    return this.http.post<UnternehmenDTO>(this.url + "/aendereUnternehmen", t, this.httpOptions)

  }

  deleteData(t: UnternehmenDTO) {
    return this.http.post<UnternehmenDTO>(this.url + "/loescheUnternehmen", t, this.httpOptions)
  }

}
