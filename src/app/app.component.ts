import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  linkPreise
  linkKontakte
  linkUnternehmen
  hover

  constructor() {
    this.linkPreise = 'active'
    this.linkKontakte = ''
    this.linkUnternehmen = ''
  }

  clickEvent(id) {
    switch (id) {
      case 1: {
        this.linkPreise = 'active'
        this.linkKontakte = ''
        this.linkUnternehmen = ''
        break;
      }
      case 2: {
        this.linkPreise = ''
        this.linkKontakte = 'active'
        this.linkUnternehmen = ''
        break;
      }
      case 3: {
        this.linkPreise = ''
        this.linkKontakte = ''
        this.linkUnternehmen = 'active'
        break;
      }
      default: {
        this.linkPreise = 'active'
        this.linkKontakte = ''
        this.linkUnternehmen = ''
        break;
      }
    }
  }
}
