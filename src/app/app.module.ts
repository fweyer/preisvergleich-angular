import './polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { DemoMaterialModule } from '../app/material-modules';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { UnternehmenTableComponent } from './components/table/unternehmentable.component';
import { UnternehmendialogComponent } from './components/dialog/unternehmendialog/unternehmendialog.component';
import { KontaktanfrageseiteComponent } from './menuComponents/kontaktanfrageseite/kontaktanfrageseite.component';
import { UnternehmenseiteComponent } from './menuComponents/unternehmenseite/unternehmenseite.component';
import { PreisseiteComponent } from './menuComponents/preisseite/preisseite.component';
import { FehlerdialogComponent } from './components/dialog/fehlerdialog/fehlerdialog.component'

@NgModule({
  declarations: [
    AppComponent,
    UnternehmenTableComponent,
    UnternehmendialogComponent,
    KontaktanfrageseiteComponent,
    UnternehmenseiteComponent,
    PreisseiteComponent,
    FehlerdialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    DemoMaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
  ],
  entryComponents: [UnternehmenTableComponent],
  bootstrap: [AppComponent],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ],
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
