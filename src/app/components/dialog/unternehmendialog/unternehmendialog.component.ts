import { Component, Inject, Input, OnDestroy } from '@angular/core';
import { OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UnternehmenTableComponent } from '../../table/unternehmentable.component'
import PreisvergleichDTO from '../../../models/PreisvergleichDTO'
import PreisvergleichService from '../../../services/preisvergleichservice.interface'

@Component({
  selector: 'app-unternehmendialog',
  templateUrl: './unternehmendialog.component.html',
  styleUrls: ['./unternehmendialog.component.scss']
})
export class UnternehmendialogComponent implements OnInit, OnDestroy {
  Object = Object;
  anzeigedaten: PreisvergleichDTO
  bearbeitungstatus: string
  dataSource: PreisvergleichDTO[]
  httpService: PreisvergleichService<PreisvergleichDTO>
  bearbeitetedaten: PreisvergleichDTO
  datentyp: PreisvergleichDTO

  constructor(
    public dialogRef: MatDialogRef<UnternehmenTableComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PreisvergleichDTO) {
    this.anzeigedaten = data["anzeigedaten"];
    this.bearbeitungstatus = data["bearbeitung"]
    this.dataSource = data["dataSource"]
    this.httpService = data["httpService"]
    this.datentyp = data["datentyp"]
  }

  ngOnInit() {
    this.bearbeitetedaten = Object.assign({}, this.anzeigedaten)
    for (let i in this.datentyp) {
      console.log("i ist : " + i)
    }
  }

  onClickhinzufuegen() {
    this.httpService.putData(this.bearbeitetedaten)
      .subscribe(
        (response) => { },
        (error) => { console.log("Ein Errorr beim hinzufügen aufgetreten ist aufgetretten") }
      )
    this.dialogRef.close();
  }

  onClickBearbeitenSpeichern() {
    this.httpService.changeData(this.bearbeitetedaten)
      .subscribe(
        (response) => {
          let index = this.dataSource.indexOf(this.anzeigedaten)
          this.dataSource.splice(index, 1, this.bearbeitetedaten)
        },
        (error) => { console.log("Ein Errorr beim ändern aufgetreten ist aufgetretten") }
      )
    this.dialogRef.close();
  }

  ngOnDestroy() { }


  changeInputValue(e) {
    this.bearbeitetedaten[e.target.name] = e.target.value
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
