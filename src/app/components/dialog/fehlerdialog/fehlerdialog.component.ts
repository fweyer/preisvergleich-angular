import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UnternehmenTableComponent } from '../../table/unternehmentable.component'

@Component({
  selector: 'app-fehlerdialog',
  templateUrl: './fehlerdialog.component.html',
  styleUrls: ['./fehlerdialog.component.scss']
})
export class FehlerdialogComponent implements OnInit {
  errorMesssage: string

  constructor(public dialogRef: MatDialogRef<UnternehmenTableComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.errorMesssage = data["error"]
  }

  ngOnInit(): void {
  }

}
