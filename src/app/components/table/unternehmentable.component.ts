import { Component, OnInit, ViewChild, OnChanges, OnDestroy, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UnternehmendialogComponent } from '../dialog/unternehmendialog/unternehmendialog.component'
import PreisvergleichDTO from '../../models/PreisvergleichDTO'
import { ExcelService } from '../../services/Excelservice'
import PreisvergleichService from '../../services/preisvergleichservice.interface';
import { MatTableDataSource } from '@angular/material/table';
import { FehlerdialogComponent } from '../dialog/fehlerdialog/fehlerdialog.component'

@Component({
  selector: 'fw-unternehmen-table',
  providers: [ExcelService],
  styleUrls: ['unternehmentable.component.scss'],
  templateUrl: 'unternehmentable.component.html',
})
export class UnternehmenTableComponent {
  @Input() columns;
  @Input() dataSource: MatTableDataSource<PreisvergleichDTO>
  @Input() datentyp;
  @Input() httpService: PreisvergleichService<PreisvergleichDTO>
  displayedColumns;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) set matSort(ms: MatSort) { this.dataSource.sort = ms; }
  constructor(public dialog: MatDialog, private excelService: ExcelService) { }


  ngOnInit() {
    this.httpService.getData()
      .subscribe(
        data => {
          this.dataSource.data = data
          this.dataSource.paginator = this.paginator;
          this.displayedColumns = this.columns.map(c => c.columnDef);
        },
        error => { console.log(error) })
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataSource.data, 'sample');
  }

  ngOnDestroy() { }

  elementEntfernen(element: PreisvergleichDTO) {
    this.ngOnDestroy()
    this.httpService.deleteData(element)
      .subscribe(
        (response) => {
          let index = this.dataSource.data.indexOf(element)
          this.dataSource.data.splice(index, 1)
          this.ngOnInit()
        },
        (error) => this.openDialogFehler("Löschen hat nicht funktioniert")
      )
  }

  openDialogHinzufuegen(bearbeitung: string): void {
    let neuesUnternehmen: PreisvergleichDTO = this.datentyp
    this.openDialog(neuesUnternehmen, bearbeitung)
  }

  openDialog(datensatz: PreisvergleichDTO, bearbeitung: string): void {
    const dialogRef = this.dialog.open(UnternehmendialogComponent, {
      width: '550px',
      height: 'auto',
      data: {
        anzeigedaten: datensatz,
        bearbeitung: bearbeitung,
        dataSource: this.dataSource.data,
        httpService: this.httpService,
        datentyp: this.datentyp
      }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnDestroy()
      this.ngOnInit()
    });

  }

  openDialogFehler(error): void {
    const dialogRef = this.dialog.open(FehlerdialogComponent, {
      width: '550px',
      height: 'auto',
      data: {
        error: error,
      }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnDestroy()
      this.ngOnInit()
    });

  }
}




/**  Copyright 2020 Google LLC. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */