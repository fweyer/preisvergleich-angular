import { Component, OnInit } from '@angular/core';
import PreisvergleichDTO from '../../models/PreisvergleichDTO'
import { PreiseDTO } from '../../models/PreiseDTO'
import { MatTableDataSource } from '@angular/material/table';
import { PreiseService } from '../../services/preise-service.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-preisseite',
  templateUrl: './preisseite.component.html',
  styleUrls: ['./preisseite.component.scss']
})
export class PreisseiteComponent implements OnInit {

  title = 'preisvergleich-angular';
  httpService: PreiseService
  columns = []
  dataSource: MatTableDataSource<PreisvergleichDTO>
  datentyp: PreisvergleichDTO = new PreiseDTO()
  PREISE_DATA: PreiseDTO[] = []
  PREISE_COLUMS = [
    { columnDef: 'position', header: 'No.', cell: (element: any) => `${element.position}` },
    { columnDef: 'datum', header: 'Datum', cell: (element: any) => `${element.datum}` },
    { columnDef: 'preis2700liter', header: 'Preis2700liter', cell: (element: any) => `${element.preis2700liter}` },
    { columnDef: 'preis4850liter', header: 'Preis4850liter', cell: (element: any) => `${element.preis4850liter}` },
    { columnDef: 'preis6400liter', header: 'Preis6400liter', cell: (element: any) => `${element.preis6400liter}` },
    { columnDef: 'plz', header: 'Plz', cell: (element: any) => `${element.plz}` },
    { columnDef: 'unternehmen', header: 'Unternehmen', cell: (element: any) => `${element.unternehmen}` },
    { columnDef: 'buttons', header: '', cell: (element: any) => `` },
  ];

  constructor(private http: HttpClient) {
    this.httpService = new PreiseService(http)
    this.columns = this.PREISE_COLUMS
    this.dataSource = new MatTableDataSource<PreisvergleichDTO>(this.PREISE_DATA);
  }

  ngOnInit(): void { }

}
