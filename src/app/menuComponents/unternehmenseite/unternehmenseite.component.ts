import { Component, OnInit } from '@angular/core';
import PreisvergleichDTO from '../../models/PreisvergleichDTO'
import { UnternehmenDTO } from '../../models/UnternehmenDTO'
import { MatTableDataSource } from '@angular/material/table';
import { UnternehmensService } from '../../services/unternehmens-service.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-unternehmenseite',
  templateUrl: './unternehmenseite.component.html',
  styleUrls: ['./unternehmenseite.component.scss']
})
export class UnternehmenseiteComponent implements OnInit {
  title = 'preisvergleich-angular';
  httpService: UnternehmensService
  columns = []
  dataSource: MatTableDataSource<PreisvergleichDTO>
  datentyp: PreisvergleichDTO = new UnternehmenDTO()
  UNTERNEHMEN_DATA: UnternehmenDTO[] = []
  UNTERNEHMEN_COLUMS = [
    { columnDef: 'position', header: 'No.', cell: (element: any) => `${element.position}` },
    { columnDef: 'name', header: 'Name', cell: (element: any) => `${element.name}` },
    { columnDef: 'addresse', header: 'Adresse', cell: (element: any) => `${element.adresse}` },
    { columnDef: 'plz', header: 'PLZ', cell: (element: any) => `${element.plz}` },
    { columnDef: 'ort', header: 'Ort', cell: (element: any) => `${element.ort}` },
    { columnDef: 'benutzername', header: 'Benutzername', cell: (element: any) => `${element.benutzername}` },
    { columnDef: 'buttons', header: '', cell: (element: any) => `` },
  ];

  constructor(private http: HttpClient) {
    this.httpService = new UnternehmensService(http)
    this.columns = this.UNTERNEHMEN_COLUMS
    this.dataSource = new MatTableDataSource<PreisvergleichDTO>(this.UNTERNEHMEN_DATA);
  }

  ngOnInit(): void {
  }
}
