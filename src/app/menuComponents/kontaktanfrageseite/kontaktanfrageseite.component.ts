import { Component, OnInit } from '@angular/core';
import PreisvergleichDTO from '../../models/PreisvergleichDTO'
import { KontaktanfragenDTO } from '../../models/KontaktanfragenDTO'
import { MatTableDataSource } from '@angular/material/table';
import { KontaktanfrageService } from '../../services/kontaktanfrage-service.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-kontaktanfrageseite',
  templateUrl: './kontaktanfrageseite.component.html',
  styleUrls: ['./kontaktanfrageseite.component.scss']
})
export class KontaktanfrageseiteComponent implements OnInit {

  title = 'preisvergleich-angular';
  httpService: KontaktanfrageService
  columns = []
  dataSource: MatTableDataSource<PreisvergleichDTO>
  datentyp: PreisvergleichDTO = new KontaktanfragenDTO()
  KONTAKT_DATA: KontaktanfragenDTO[] = []
  KONTAKT_COLUMS = [
    { columnDef: 'position', header: 'No.', cell: (element: any) => `${element.position}` },
    { columnDef: 'firmennamen', header: 'firmennamen', cell: (element: any) => `${element.firmennamen}` },
    { columnDef: 'firmenadresse', header: 'Firmenadresse', cell: (element: any) => `${element.firmenadresse}` },
    { columnDef: 'plz', header: 'plz', cell: (element: any) => `${element.plz}` },
    { columnDef: 'ort', header: 'ort', cell: (element: any) => `${element.ort}` },
    { columnDef: 'kontaktperson', header: 'kontaktperson', cell: (element: any) => `${element.kontaktperson}` },
    { columnDef: 'email', header: 'EMail', cell: (element: any) => `${element.email}` },
    { columnDef: 'betreff', header: 'Betreff', cell: (element: any) => `${element.betreff}` },
    { columnDef: 'nachricht', header: '', cell: (element: any) => `` },
    { columnDef: 'buttons', header: '', cell: (element: any) => `` },
  ];

  constructor(private http: HttpClient) {
    this.httpService = new KontaktanfrageService(http)
    this.columns = this.KONTAKT_COLUMS
    this.dataSource = new MatTableDataSource<PreisvergleichDTO>(this.KONTAKT_DATA);
  }

  ngOnInit(): void { }
}
