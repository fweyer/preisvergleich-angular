import PreisvergleichDTO from '../models/PreisvergleichDTO'

export class PreiseDTO extends PreisvergleichDTO {
  position: number = 0
  ID: number = 0;
  datum: string = '';
  preis2700liter: string = '';
  preis4850liter: string = '';
  preis6400liter: string = '';
  plz: string = ''
  unternehmen: string = ''
}