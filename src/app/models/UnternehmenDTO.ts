import PreisvergleichDTO from '../models/PreisvergleichDTO'


export class UnternehmenDTO extends PreisvergleichDTO {
  position: number = 0
  ID: number = 0;
  name: string = '';
  adresse: string = '';
  plz: string = '';
  ort: string = '';
  benutzername: string = ''
}


