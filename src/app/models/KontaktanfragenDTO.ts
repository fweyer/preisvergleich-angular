import PreisvergleichDTO from '../models/PreisvergleichDTO'

export class KontaktanfragenDTO extends PreisvergleichDTO {
  position: number = 0
  ID: number = 0;
  firmennamen: string = '';
  firmenadresse: string = '';
  plz: string = '';
  ort: string = '';
  kontaktperson: string = '';
  email: string = '';
  betreff: string = '';
  nachricht: string = ''
}